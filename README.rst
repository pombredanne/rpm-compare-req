===============
rpm-compare-req
===============

rpm-compare-req is a tool for comparing dependencies of an RPM against a set of repositories.

rpm-compare-req is licensed under MIT license.
