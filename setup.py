#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from setuptools import setup, find_packages

if sys.version_info < (2, 7):
    install_requires = ['argparse']
else:
    install_requires = []

description = """rpm-compare-req is a tool for comparing dependencies of an RPM against a set of repositories."""

setup(
    name = 'rpm-compare-req',
    version = '0.1.0',
    description = "Compare dependencies of RPM against set of repos.",
    long_description = description,
    keywords = 'rpm, repository, requirements, dependencies',
    author = 'Bohuslav "Slavek" Kabrda',
    author_email = 'bkabrda@redhat.com',
    url = 'https://bitbucket.org/bkabrda/rpm-compare-req/',
    license = 'MIT',
    packages = ['rpm_compare_req'],
    setup_requires = [],
    install_requires = install_requires,
    entry_points={'console_scripts':['rpm-compare-req = rpm_compare_req.bin:main']},
    test_suite='test',
    classifiers = ['Development Status :: 4 - Beta',
                   'Environment :: Console',
                   'Intended Audience :: Developers',
                   'Intended Audience :: System Administrators',
                   'License :: OSI Approved :: MIT License',
                   'Operating System :: POSIX :: Linux',
                   'Programming Language :: Python',
                   'Topic :: System :: Software Distribution',
                  ]
)
