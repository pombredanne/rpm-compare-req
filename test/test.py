import unittest
import subprocess
import os

class TestCase(unittest.TestCase):
    #Specially crafted BuildRequires (e.g. BuildRequires: a`touch${IFS}"test"`)
    #could result in arbitrary code execution
    def test_shell_injection(self):
        if os.path.exists("I_shouldnt_be_here"):
            os.remove("I_shouldnt_be_here")

        subprocess.call(["rpm-compare-req", \
            os.path.join("test", "shell-injection.src.rpm")])
        self.assertFalse(os.path.exists("I_shouldnt_be_here"))

        if os.path.exists("I_shouldnt_be_here"):
            os.remove("I_shouldnt_be_here")
