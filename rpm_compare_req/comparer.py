from __future__ import print_function
import rpm
import subprocess
import sys

class Comparer(object):
    def __init__(self, verbose = True, libs_only = False, print_providers = False, repos = [], package = ''):
        self.verbose = verbose
        self.libs_only = libs_only
        self.print_providers = print_providers
        if repos:
            self.repos = repos if isinstance(repos, list) else [repos]
        else:
            self.repos = []
        self.package = package
        self.repoquery = self.construct_repoquery()

    @property
    def headers(self):
        transaction = rpm.TransactionSet()
        file_desc = open(self.package)
        return transaction.hdrFromFdno(file_desc)

    @property
    def requires(self):
        if not hasattr(self, '_requires'):
            requires = self.headers[rpm.RPMTAG_REQUIRENEVRS]
            if self.libs_only:
                requires = [r for r in requires if r.find('.so') != -1]
            self._requires = [r for r in requires if not r.startswith('rpmlib(')]

        return self._requires

    def get_normalized_repo_name(self, name):
        return name.translate(None, ':/&?%.-_')

    @property
    def repo_dict(self):
        result = {}
        for repo in self.repos:
            result[self.get_normalized_repo_name(repo)] = repo

        return result

    def construct_repoquery(self):
        repoquery = ['/usr/bin/repoquery']

        repo_dict = self.repo_dict
        for k, v in repo_dict.items():
            repoquery.append('--repofrompath={0},{1}'.format(k, v))
            repoquery.append('--repoid={0}'.format(k))

        repoquery.append('--verbose' if self.verbose else '--quiet')
        repoquery.append('--whatprovides')

        return repoquery

    def get_repoquery_for(self, requirement):
        return self.repoquery + [requirement]

    def find_providers(self):
        providers = []

        for r in self.requires:
            rq_string = self.get_repoquery_for(r)
            rq_call = subprocess.Popen(rq_string,
                                       stdout = subprocess.PIPE,
                                       stderr = subprocess.PIPE)
            rq_result = rq_call.communicate()
            self.verbose and print(rq_string)

            if rq_result[1] and not rq_result[1].startswith('Added '): # there was an error running repoquery
                print('Error getting provider for "{0}": {1}'.format(r, rq_result[1]), file = sys.stderr)
            providers.append(rq_result[0].strip().replace('\n', ', '))

        return providers

    def compare(self):
        providers = self.find_providers()
        for i, r in enumerate(self.requires):
            if self.print_providers: # print all requires with their providers
                if providers[i]:
                    print('{0} - {1}'.format(r, providers[i]))
                else:
                    print('{0} - MISSING PROVIDER'.format(r))
            else: # print only those without provider
                if not providers[i]:
                    print(r)

        # return the proper bash code
        return 1 if '' in providers else 0
