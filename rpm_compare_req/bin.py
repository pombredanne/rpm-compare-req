"""Compares Requires of an RPM against a repository.
By default, it prints a list of requirements, that weren't satisfied by any provider -
this behaviour can be changed by various switches.

Returncodes:
    0 - everything went ok and all the package requirements were satisfied
    1 - everything went ok, but some requirements aren't satisfied
    2 - bad invocation
"""

import argparse
import sys

from rpm_compare_req.comparer import Comparer

### Parse arguments

def main():
    parser = argparse.ArgumentParser(description = 'Compare Requires of an RPM against a \
                                                    repository. Prints all the unsatisfied \
                                                    requirements by default.')
    parser.add_argument('-v', '--verbose',
                        required = False,
                        help = 'Prints out repoquery calls and other useful information.',
                        action = 'store_true'
                       )
    parser.add_argument('-l', '--libs-only',
                        required = False,
                        help = 'Will only check for library soabi dependencies.',
                        action = 'store_true'
                       )
    parser.add_argument('-p', '--print-providers',
                        required = False,
                        help = 'Will print the providers who satisfy given dependency.',
                        action = 'store_true'
                       )
    parser.add_argument('-r', '--repofrompath',
                        required = False,
                        action = 'append',
                        help = 'Repo to pass to repoquery, can be specified multiple times.',
                       )
    parser.add_argument('package',
                        help = 'Package to check (single RPM or SRPM).',
                        metavar = 'PACKAGE',
                       )

    args = parser.parse_args()

    comparer = Comparer(verbose = args.verbose,
                        libs_only = args.libs_only,
                        print_providers = args.print_providers,
                        repos = args.repofrompath,
                        package = args.package)

    sys.exit(comparer.compare())
